extern crate hex;

use blurz::BluetoothSession;
use clap::{App, Arg};
use std::thread;
use std::time::Duration;

mod ledble;

const LED_PATH: &'static str = "/org/bluez/hci0/dev_FF_FF_30_05_75_08";

fn main() {
    let matches = App::new(clap::crate_name!())
        .author(clap::crate_authors!())
        .version(clap::crate_version!())
        .about(clap::crate_description!())
        .arg(
            Arg::with_name("name")
                .short("n")
                .takes_value(true)
                .default_value(&LED_PATH),
        )
        .arg(Arg::with_name("color").short("c").takes_value(true))
        .arg(
            Arg::with_name("on")
                .short("o")
                .conflicts_with("off")
                .conflicts_with("color"),
        )
        .arg(
            Arg::with_name("off")
                .short("f")
                .conflicts_with("on")
                .conflicts_with("color"),
        )
        .get_matches();

    let name = matches.value_of("name").unwrap();
    let color = matches.value_of("color").unwrap_or("");
    let c = matches.is_present("color");
    let on = matches.is_present("on");
    let off = matches.is_present("off");

    let bt_session = &BluetoothSession::create_session(None).unwrap();

    let led = ledble::LedBle::new(bt_session, name.to_string());
    if let Err(e) = led.device.connect(1000) {
        panic!("Failed to connect {:?}: {:?}", led.device.get_id(), e);
    } else {
        println!("Connected to {:?}", led.device.get_id());
        println!("Checking gatt...");
        // We need to wait a bit after calling connect to safely
        // get the gatt services
        thread::sleep(Duration::from_millis(5000));
        if on {
            led.on().unwrap();
        } else if off {
            led.off().unwrap();
        } else if c {
            let rgb =hex::decode(color).unwrap();
            led.set_color(rgb[0], rgb[1], rgb[2]).unwrap();
        }
    }
    println!("Done.");
    led.device.disconnect().unwrap();
}
