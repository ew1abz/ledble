use blurz::{BluetoothDevice, BluetoothGATTCharacteristic, BluetoothSession};
use std::error::Error;

const CONTROL_CHAR: &'static str = "/service0005/char0006";

pub struct LedBle<'a> {
    pub device: BluetoothDevice<'a>,
    control: BluetoothGATTCharacteristic<'a>,
}

impl<'a> LedBle<'a> {
    pub fn new(session: &BluetoothSession, name: String) -> LedBle {
        let d = BluetoothDevice::new(session, name);
        let c = BluetoothGATTCharacteristic::new(session, d.get_id() + CONTROL_CHAR);

        LedBle {
            device: d,
            control: c,
        }
    }

    pub fn on(&self) -> Result<(), Box<dyn Error>> {
        self.control.write_value(vec![0xcc, 0x23, 0x33], None)
    }

    pub fn off(&self) -> Result<(), Box<dyn Error>> {
        self.control.write_value(vec![0xcc, 0x24, 0x33], None)
    }

    pub fn set_color(&self, r: u8, g: u8, b: u8) -> Result<(), Box<dyn Error>> {
        self.control
            .write_value(vec![0x56, r, g, b, 0x00, 0xf0, 0xaa], None)
    }
}
